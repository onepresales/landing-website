OnePresales - Landing Website
===

This is a website running on a Express NodeJS Server.

# Requirements
* Nodejs (https://nodejs.org/en/) - 6.11.4 TLS
* Install Bower globally (only once)

```shell
npm install bower -g
```

# Quick Start

## Install dependencies

Server dependencies
```shell
npm install
```

Client dependencies
```shell
bower install
```

## Run it

```shell
npm start
```

Access: http://localhost:9090/

