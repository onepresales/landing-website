const onepresalesServer = require('onepresales-server');
const path = require('path');

let staticRoot = path.resolve(__dirname, '..', 'public');

onepresalesServer.start({
	secure: false,
	port: 9090,
	staticRoot: staticRoot,
	relayState: ''
});

var app = onepresalesServer.app;
app.get("/api/ping", function(req, res){
	res.send("pong");
});