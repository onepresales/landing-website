const express = require("express");
const app = express();

app.use('/', express.static(__dirname + '/../underconstruction'));

const server = app.listen(9090, () => {
	console.log(`Server listening on port 9090`)
});