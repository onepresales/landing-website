# Stage 1: get sources from npm and git over ssh
FROM node:8.9.4 AS sources
ARG SSH_KEY
ARG SSH_KEY_PASSPHRASE

    
RUN git clone https://gitlab.com/onepresales/landing-website.git /app

WORKDIR /app

RUN npm install --production && \
    npm install bower -g && \
    bower install --allow-root 

# Stage 2: build minified production code
FROM node:8.9.4-alpine AS production
WORKDIR /app
COPY --from=sources /app/ ./
COPY . /app/

EXPOSE 9090
CMD ["node", "server/index"]