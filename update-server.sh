#!/bin/bash

SERVER="one-staging"
TOOL="golden"
MSG="Format: update-server.sh [dev|prod ] [landing|qmi|golden|qhc|slides|metrics|pptexplorer]"

if [ "$1" == "dev" ] || [ "$1" == "prod" ]; then
    if [ "$1" == "prod" ]; then
    	SERVER="one-production"
    fi
else
    echo ${MSG}
    exit 1
fi

if [ "$2" == "landing" ] || [ "$2" == "qmi" ] || [ "$2" == "golden" ] || [ "$2" == "qhc" ] || [ "$2" == "slides" ] || [ "$2" == "metrics" ] || [ "$2" == "pptexplorer" ]; then
    TOOL=$2
else
    echo ${MSG}
    exit 1
fi

echo "Executing: ssh -i ~/.ssh/gear-vagrant.pem ubuntu@${SERVER} /home/ubuntu/update-${TOOL}-website.sh"

ssh -i ~/.ssh/gear-vagrant.pem ubuntu@${SERVER} /home/ubuntu/update-${TOOL}-website.sh



